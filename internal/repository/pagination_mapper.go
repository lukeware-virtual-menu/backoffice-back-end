package repository

type PaginationMapper[T any] struct {
	Total     int64 `json:"total,omitempty"`
	Page      int64 `json:"page,omitempty"`
	PerPage   int64 `json:"perPage,omitempty"`
	Prev      int64 `json:"prev,omitempty"`
	Next      int64 `json:"next,omitempty"`
	TotalPage int64 `json:"totalPage,omitempty"`
	Data      []*T  `json:"data,omitempty"`
}
