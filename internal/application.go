package internal

import (
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/order_module/api_order"
	apiPlate "gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/plate_module/api_plate"
	"log"
	"net/http"
	"os"

	"github.com/go-playground/validator"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

const HttpPort string = "1323"

type Application struct {
}

type CustomValidator struct {
	validator *validator.Validate
}

func NewApp() *Application {
	return new(Application)
}

func (app Application) Init() {
	log.Println("Application init")
	e := echo.New()

	log.Println("create logger manager")
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${time_rfc3339_nano} ${protocol} ${method} '${host}${uri}' status=${status}\n",
	}))

	log.Println("Create json validator")
	e.Validator = &CustomValidator{validator: validator.New()}

	log.Println("create path: /api/v1")
	group := e.Group("/api/v1")

	log.Println("Create service apiOrder")
	app.createPlateResource(group)
	app.createOrderResource(group)

	port := os.Getenv("GRPC_PORT")

	if port == "" {
		e.Logger.Fatal(e.Start(":" + HttpPort))
	} else {
		e.Logger.Fatal(e.Start(":" + port))
	}
}

func (app Application) createPlateResource(e *echo.Group) {
	plateRest := apiPlate.NewPlateApi()
	e.POST("/plates", plateRest.Save)
	e.GET("/plates", plateRest.FindAll)
}

func (app Application) createOrderResource(e *echo.Group) {
	orderRest := api_order.NewOrderApi()
	e.POST("/orders", orderRest.Save)
	e.GET("/orders", orderRest.FindAll)
	e.GET("/orders/:uuid", orderRest.FindByUUID)
	e.PUT("/orders/:uuid", orderRest.Update)
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		log.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return nil
}
