package main

import (
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal"
)

func main() {
	internal.NewApp().Init()
}
