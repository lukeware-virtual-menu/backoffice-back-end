package entity

import "time"

type Order struct {
	Items       []Item    `json:"items"`
	Note        string    `json:"note"`
	SubTotal    float64   `json:"subTotal"`
	TableCode   string    `json:"tableCode"`
	IsFinished  bool      `json:"isFinished"`
	IsInitiated bool      `json:"isInitiated"`
	CreateAt    time.Time `json:"createAt"`
}
