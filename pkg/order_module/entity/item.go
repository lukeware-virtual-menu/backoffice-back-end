package entity

type Item struct {
	HouseSpecialty bool    `json:"houseSpecialty"`
	Title          string  `json:"title"`
	Description    string  `json:"description"`
	Price          float64 `json:"price"`
	Category       string  `json:"category"`
	Quantity       int     `json:"quantity"`
	Note           string  `json:"note"`
	Check          bool    `json:"check"`
}
