package mapper

type CreateItemMapper struct {
	Sequence       int     `bson:"sequence" json:"sequence"`
	HouseSpecialty bool    `bson:"house_specialty" json:"houseSpecialty"`
	Title          string  `bson:"title" json:"title"`
	Description    string  `bson:"description" json:"description"`
	Price          float64 `bson:"price" json:"price"`
	Category       string  `bson:"category" json:"category"`
	Quantity       int     `bson:"quantity" json:"quantity"`
	Note           string  `bson:"note" json:"note"`
	Check          bool    `bson:"check" json:"check"`
}
