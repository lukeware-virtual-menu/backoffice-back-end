package mapper

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type OrderMapper struct {
	Uuid        primitive.ObjectID `bson:"_id" json:"uuid"`
	Items       []CreateItemMapper `bson:"items" json:"items"`
	Note        string             `bson:"note" json:"note"`
	SubTotal    float64            `bson:"sub_total" json:"subTotal"`
	TableCode   string             `bson:"table_code" json:"tableCode"`
	IsFinished  bool               `bson:"is_finished" json:"isFinished"`
	IsInitiated bool               `bson:"is_initiated" json:"isInitiated"`
	CreateAt    time.Time          `bson:"create_at" json:"createAt"`
}
