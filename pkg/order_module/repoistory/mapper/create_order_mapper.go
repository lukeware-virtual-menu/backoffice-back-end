package mapper

import "go.mongodb.org/mongo-driver/bson/primitive"

type CreateOrderMapper struct {
	Items       []CreateItemMapper `bson:"items"`
	Note        string             `bson:"note"`
	SubTotal    float64            `bson:"sub_total"`
	TableCode   string             `bson:"table_code"`
	IsFinished  bool               `bson:"is_finished"`
	IsInitiated bool               `bson:"is_initiated"`
	CreateAt    primitive.DateTime `bson:"create_at"`
}
