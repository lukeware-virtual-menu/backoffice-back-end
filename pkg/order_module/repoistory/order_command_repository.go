package repository

import (
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/database"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/repository"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/order_module/repoistory/mapper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IOrderCommandRepository interface {
	Save(mapper mapper.CreateOrderMapper) (*string, error)
	Update(mapper mapper.UpdateOrderMapper) error
	Delete(uuid string) error
}

type orderCommandRepository struct {
	repository.AbstractRepository[mapper.CreateOrderMapper]
}

func (c orderCommandRepository) Save(mapper mapper.CreateOrderMapper) (*string, error) {
	save, err := c.AbstractRepository.Save(mapper)
	if err != nil {
		return nil, err
	}
	return &save, nil
}

func (c orderCommandRepository) Update(mapper mapper.UpdateOrderMapper) error {
	err := c.AbstractRepository.Update(mapper.Uuid, mapper)
	if err != nil {
		return err
	}
	return nil
}

func (c orderCommandRepository) Delete(uuid string) error {
	hex, _ := primitive.ObjectIDFromHex(uuid)
	err := c.AbstractRepository.Delete(hex)
	if err != nil {
		return err
	}
	return nil
}

func NewOrderCommandRepository() IOrderCommandRepository {
	orderRepository := orderCommandRepository{
		repository.AbstractRepository[mapper.CreateOrderMapper]{
			MongoDataSource: database.NewMongoDataSource(),
			DatabaseName:    databaseName,
			TableName:       tableName,
		},
	}
	return &orderRepository
}
