package repository

import (
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/database"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/repository"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/order_module/repoistory/mapper"
	"go.mongodb.org/mongo-driver/bson"
)

type IOrderQueryRepository interface {
	FindAll(page int64, size int64) (*repository.PaginationMapper[mapper.OrderMapper], error)
	FindByUuid(uuid string) (*mapper.OrderMapper, error)
}

type orderQueryRepository struct {
	repository.AbstractRepository[mapper.OrderMapper]
}

func (c *orderQueryRepository) FindByUuid(uuid string) (*mapper.OrderMapper, error) {
	return c.AbstractRepository.FindById(uuid)
}

func (c *orderQueryRepository) FindAll(page int64, size int64) (*repository.PaginationMapper[mapper.OrderMapper], error) {

	filters := bson.D{
		{Key: "$and",
			Value: bson.A{
				bson.D{{Key: "is_finished", Value: bson.D{{Key: "$eq", Value: false}}}},
			},
		},
	}

	paginatedData, orderMapper, err := c.AbstractRepository.FindAllFilter(int(page), int(size), filters)
	if err != nil {
		return nil, err
	}
	return &repository.PaginationMapper[mapper.OrderMapper]{
		Data:      orderMapper,
		Next:      paginatedData.Pagination.Next,
		Page:      paginatedData.Pagination.Page,
		Prev:      paginatedData.Pagination.Prev,
		Total:     paginatedData.Pagination.Total,
		PerPage:   paginatedData.Pagination.PerPage,
		TotalPage: paginatedData.Pagination.TotalPage,
	}, nil
}

func NewOrderQueryRepository() IOrderQueryRepository {
	orderRepository := orderQueryRepository{
		repository.AbstractRepository[mapper.OrderMapper]{
			MongoDataSource: database.NewMongoDataSource(),
			DatabaseName:    databaseName,
			TableName:       tableName,
		},
	}
	return &orderRepository
}
