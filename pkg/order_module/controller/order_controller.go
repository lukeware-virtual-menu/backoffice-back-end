package controller

import (
	internalRepository "gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/repository"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/order_module/entity"
	repository "gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/order_module/repoistory"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/order_module/repoistory/mapper"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type IOrderController interface {
	Save(order entity.Order) (err error)
	FindAll(page int64, limit int64) (*internalRepository.PaginationMapper[mapper.OrderMapper], error)
	FindByUuid(uuid string) (*mapper.OrderMapper, error)
	Update(uuid string, order entity.Order) error
}

type orderController struct {
	orderCommandRepository repository.IOrderCommandRepository
	orderQueryRepository   repository.IOrderQueryRepository
}

func NewOrderController() IOrderController {
	controller := orderController{
		orderCommandRepository: repository.NewOrderCommandRepository(),
		orderQueryRepository:   repository.NewOrderQueryRepository(),
	}
	return &controller
}

func (p *orderController) Save(order entity.Order) (err error) {
	createOrder := mapper.CreateOrderMapper{
		Note:        order.Note,
		SubTotal:    order.SubTotal,
		Items:       getItems(order.Items),
		TableCode:   order.TableCode,
		IsFinished:  false,
		IsInitiated: false,
		CreateAt:    primitive.NewDateTimeFromTime(time.Now()),
	}
	_, err = p.orderCommandRepository.Save(createOrder)
	if err != nil {
		return err
	}
	return nil
}
func (p *orderController) Update(uuid string, order entity.Order) error {
	hex, _ := primitive.ObjectIDFromHex(uuid)
	updateOrder := mapper.UpdateOrderMapper{
		Uuid:        hex,
		Note:        order.Note,
		SubTotal:    order.SubTotal,
		Items:       getItems(order.Items),
		TableCode:   order.TableCode,
		IsFinished:  order.IsFinished,
		IsInitiated: order.IsInitiated,
		CreateAt:    primitive.NewDateTimeFromTime(time.Now()),
	}
	err := p.orderCommandRepository.Update(updateOrder)
	if err != nil {
		return err
	}
	return nil
}

func (p *orderController) FindAll(page int64, limit int64) (*internalRepository.PaginationMapper[mapper.OrderMapper], error) {
	return p.orderQueryRepository.FindAll(page, limit)
}

func (p *orderController) FindByUuid(uuid string) (*mapper.OrderMapper, error) {
	return p.orderQueryRepository.FindByUuid(uuid)
}

func getItems(items []entity.Item) []mapper.CreateItemMapper {
	var createItems []mapper.CreateItemMapper
	sequence := 0
	for _, item := range items {
		sequence += 1
		createItem := mapper.CreateItemMapper{
			Title:          item.Title,
			Description:    item.Description,
			HouseSpecialty: item.HouseSpecialty,
			Note:           item.Note,
			Price:          item.Price,
			Category:       item.Category,
			Quantity:       item.Quantity,
			Sequence:       sequence,
			Check:          item.Check,
		}
		createItems = append(createItems, createItem)
	}
	return createItems
}
