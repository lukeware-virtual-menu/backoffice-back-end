package api_order

import (
	"github.com/labstack/echo"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/order_module/controller"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/order_module/entity"
	"log"
	"net/http"
	"strconv"
)

type IOrderApi interface {
	Save(c echo.Context) (err error)
	Update(c echo.Context) (err error)
	FindAll(c echo.Context) (err error)
	FindByUUID(c echo.Context) (err error)
}

type orderApi struct {
	orderController controller.IOrderController
}

func NewOrderApi() IOrderApi {
	api := orderApi{
		orderController: controller.NewOrderController(),
	}
	return &api
}

func (o *orderApi) Save(c echo.Context) (err error) {
	plateRequest := new(entity.Order)
	if err = c.Bind(plateRequest); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	if err = c.Validate(plateRequest); err != nil {
		log.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, "verifique se todos os campos obrigatórios foram preenchidos")
	}
	err = o.orderController.Save(*plateRequest)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, nil)
}

func (o *orderApi) FindAll(c echo.Context) (err error) {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		page = 1
	}
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		limit = 20
	}
	plates, err := o.orderController.FindAll(int64(page), int64(limit))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, plates)
}

func (o *orderApi) FindByUUID(c echo.Context) (err error) {
	uuid := c.Param("uuid")
	order, err := o.orderController.FindByUuid(uuid)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, order)
}

func (o *orderApi) Update(c echo.Context) (err error) {
	plateRequest := new(entity.Order)
	uuid := c.Param("uuid")
	if err = c.Bind(plateRequest); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	if err = c.Validate(plateRequest); err != nil {
		log.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, "verifique se todos os campos obrigatórios foram preenchidos")
	}
	err = o.orderController.Update(uuid, *plateRequest)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, nil)
}
