package apiPlate

import (
	"github.com/labstack/echo"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/plate_module/controller"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/plate_module/entity"
	"log"
	"net/http"
	"strconv"
)

type IPlateApi interface {
	Save(c echo.Context) (err error)
	FindAll(c echo.Context) (err error)
}

type plateApi struct {
	plateController controller.IPlateController
}

func NewPlateApi() IPlateApi {
	api := plateApi{
		plateController: controller.NewPlateController(),
	}
	return &api
}

func (o *plateApi) Save(c echo.Context) (err error) {
	plateRequest := new(entity.Plate)
	if err = c.Bind(plateRequest); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	if err = c.Validate(plateRequest); err != nil {
		log.Println(err)
		return echo.NewHTTPError(http.StatusBadRequest, "verifique se todos os campos obrigatórios foram preenchidos")
	}
	err = o.plateController.Save(*plateRequest)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, nil)
}

func (o *plateApi) FindAll(c echo.Context) (err error) {
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		page = 1
	}
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		limit = 20
	}
	plates, err := o.plateController.FindAll(int64(page), int64(limit))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, plates)
}
