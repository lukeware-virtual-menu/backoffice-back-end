package entity

type Plate struct {
	Image          string  `json:"image"`
	HouseSpecialty bool    `json:"houseSpecialty"`
	Title          string  `json:"title"`
	Description    string  `json:"description"`
	Price          float64 `json:"price"`
	Category       string  `json:"category"`
	Enable         bool    `json:"enable"`
}
