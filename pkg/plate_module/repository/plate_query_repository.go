package repository

import (
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/database"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/repository"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/plate_module/repository/mapper"
)

type IPlateQueryRepository interface {
	FindById(uuid string) (*mapper.PlateMapper, error)
	FindAll(page int64, size int64) (*repository.PaginationMapper[mapper.PlateMapper], error)
}

type plateQueryRepository struct {
	repository.AbstractRepository[mapper.PlateMapper]
}

func (c *plateQueryRepository) FindById(uuid string) (*mapper.PlateMapper, error) {
	return c.AbstractRepository.FindById(uuid)
}

func (c *plateQueryRepository) FindAll(page int64, size int64) (*repository.PaginationMapper[mapper.PlateMapper], error) {
	paginatedData, plateMapper, err := c.AbstractRepository.FindAll(int(page), int(size))
	if err != nil {
		return nil, err
	}
	return &repository.PaginationMapper[mapper.PlateMapper]{
		Data:      plateMapper,
		Next:      paginatedData.Pagination.Next,
		Page:      paginatedData.Pagination.Page,
		Prev:      paginatedData.Pagination.Prev,
		Total:     paginatedData.Pagination.Total,
		PerPage:   paginatedData.Pagination.PerPage,
		TotalPage: paginatedData.Pagination.TotalPage,
	}, nil
}

func NewPlateQueryRepository() IPlateQueryRepository {
	plateRepository := plateQueryRepository{
		repository.AbstractRepository[mapper.PlateMapper]{
			MongoDataSource: database.NewMongoDataSource(),
			DatabaseName:    databaseName,
			TableName:       tableName,
		},
	}
	return &plateRepository
}
