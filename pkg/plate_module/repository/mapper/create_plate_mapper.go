package mapper

type CreatePlateMapper struct {
	Image          string  `bson:"image"`
	HouseSpecialty bool    `bson:"house_specialty"`
	Title          string  `bson:"title"`
	Description    string  `bson:"description"`
	Price          float64 `bson:"price"`
	Category       string  `bson:"category"`
	Enable         bool    `bson:"enable"`
}
