package mapper

import "go.mongodb.org/mongo-driver/bson/primitive"

type PlateMapper struct {
	Uuid           primitive.ObjectID `bson:"_id" json:"id"`
	Image          string             `bson:"image" json:"image"`
	HouseSpecialty bool               `bson:"house_specialty" json:"houseSpecialty"`
	Title          string             `bson:"title" json:"title"`
	Description    string             `bson:"description" json:"description"`
	Price          float64            `bson:"price" json:"price"`
	Category       string             `bson:"category" json:"category"`
	Enable         bool               `bson:"enable" json:"enable"`
}
