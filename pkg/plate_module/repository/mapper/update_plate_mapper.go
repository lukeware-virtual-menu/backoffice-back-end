package mapper

import "go.mongodb.org/mongo-driver/bson/primitive"

type UpdatePlateMapper struct {
	CreatePlateMapper
	Uuid primitive.ObjectID `bson:"_id"`
}
