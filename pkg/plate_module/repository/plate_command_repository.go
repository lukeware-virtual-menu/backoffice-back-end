package repository

import (
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/database"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/repository"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/plate_module/repository/mapper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type IPlateCommandRepository interface {
	Save(mapper mapper.CreatePlateMapper) (*string, error)
	Update(mapper mapper.UpdatePlateMapper) error
	Delete(uuid string) error
}

type plateCommandRepository struct {
	repository.AbstractRepository[mapper.CreatePlateMapper]
}

func (c plateCommandRepository) Save(mapper mapper.CreatePlateMapper) (*string, error) {
	save, err := c.AbstractRepository.Save(mapper)
	if err != nil {
		return nil, err
	}
	return &save, nil
}

func (c plateCommandRepository) Update(mapper mapper.UpdatePlateMapper) error {
	err := c.AbstractRepository.Update(mapper.Uuid, mapper)
	if err != nil {
		return err
	}
	return nil
}

func (c plateCommandRepository) Delete(uuid string) error {
	hex, _ := primitive.ObjectIDFromHex(uuid)
	err := c.AbstractRepository.Delete(hex)
	if err != nil {
		return err
	}
	return nil
}

func NewPlateCommandRepository() IPlateCommandRepository {
	plateRepository := plateCommandRepository{
		repository.AbstractRepository[mapper.CreatePlateMapper]{
			MongoDataSource: database.NewMongoDataSource(),
			DatabaseName:    databaseName,
			TableName:       tableName,
		},
	}
	return &plateRepository
}
