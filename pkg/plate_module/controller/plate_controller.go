package controller

import (
	internalRepository "gitlab.com/lukeware-virtual-menu/backoffice-back-end/internal/repository"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/plate_module/entity"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/plate_module/repository"
	"gitlab.com/lukeware-virtual-menu/backoffice-back-end/pkg/plate_module/repository/mapper"
)

type IPlateController interface {
	Save(plate entity.Plate) (err error)
	FindAll(page int64, limit int64) (*internalRepository.PaginationMapper[mapper.PlateMapper], error)
}

type plateController struct {
	plateCommandRepository repository.IPlateCommandRepository
	plateQueryRepository   repository.IPlateQueryRepository
}

func NewPlateController() IPlateController {
	controller := plateController{
		plateCommandRepository: repository.NewPlateCommandRepository(),
		plateQueryRepository:   repository.NewPlateQueryRepository(),
	}
	return &controller
}

func (p *plateController) Save(plate entity.Plate) (err error) {
	createPlate := mapper.CreatePlateMapper{
		Category:       plate.Category,
		Description:    plate.Description,
		Enable:         plate.Enable,
		HouseSpecialty: plate.HouseSpecialty,
		Image:          plate.Image,
		Price:          plate.Price,
		Title:          plate.Title,
	}
	_, err = p.plateCommandRepository.Save(createPlate)
	if err != nil {
		return err
	}
	return nil
}

func (p *plateController) FindAll(page int64, limit int64) (*internalRepository.PaginationMapper[mapper.PlateMapper], error) {
	return p.plateQueryRepository.FindAll(page, limit)
}
