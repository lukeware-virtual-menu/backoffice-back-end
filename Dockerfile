FROM golang:latest AS build
WORKDIR /app
COPY . .

ARG GRPC_PORT
ARG DATABASE_MONGODB_URL
ENV DATABASE_MONGODB_URL=${DATABASE_MONGODB_URL}

RUN go mod download
RUN cd cmd && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /server

FROM gcr.io/distroless/static-debian11:latest
WORKDIR /app
COPY --from=build server /server
EXPOSE 1323
USER nonroot:nonroot
ENTRYPOINT ["/server"]
